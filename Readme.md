General architecture:
* separated into at-least 2 layers:
    * public = publicly accessible domain
    * private = internally/privately accessible
* API1 may connect to API2, but through corresponding SGW
* API-names must be meaningful (domain-specific)

![architecture.png](architecture.png)