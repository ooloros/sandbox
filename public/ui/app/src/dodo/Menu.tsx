import * as React from "react";


class Menu extends React.Component {
    render() {
        return (
            <ol>
                <li>Pitsad</li>
                <li>Snäkid</li>
                <li>Magustoidud</li>
                <li>Pakkumised</li>
            </ol>
        );
    }
}
export default Menu;