package ee.ooloros.uiback.basket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


class BasketTest {
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  @Test
  void shouldSerializeBasket() throws JsonProcessingException {
    String expectedJson = "{\"uuid\":\"b7a05aaf-05a1-4dc2-9e8f-66ee01d28cb2\","
                          + "\"basketItems\":[{\"uuid\":\"2cd0de9e-7b36-4561-9644-b1f719e186ae\"}]}";
    Basket basket = new Basket("b7a05aaf-05a1-4dc2-9e8f-66ee01d28cb2",
      Collections.singletonList(new BasketItem("2cd0de9e-7b36-4561-9644-b1f719e186ae")));
    String actualJson = OBJECT_MAPPER.writeValueAsString(basket);
    assertThat(actualJson, is(expectedJson));
  }

  @Test
  void shouldDeserializeBasket() throws JsonProcessingException {
    String basketJson = "{\"uuid\":\"b7a05aaf-05a1-4dc2-9e8f-66ee01d28cb2\","
                        + "\"basketItems\":[{\"uuid\":\"2cd0de9e-7b36-4561-9644-b1f719e186ae\"}]}";
    Basket expectedBasket = new Basket("b7a05aaf-05a1-4dc2-9e8f-66ee01d28cb2",
      Collections.singletonList(new BasketItem("2cd0de9e-7b36-4561-9644-b1f719e186ae")));
    Basket actualBasket = OBJECT_MAPPER.readValue(basketJson, Basket.class);
    assertThat(actualBasket.getUuid(), is(expectedBasket.getUuid()));
    assertThat(actualBasket.getBasketItems().size(), is(expectedBasket.getBasketItems().size()));
    assertThat(actualBasket.getBasketItems().get(0).getUuid(), is(expectedBasket.getBasketItems().get(0).getUuid()));
  }
}
