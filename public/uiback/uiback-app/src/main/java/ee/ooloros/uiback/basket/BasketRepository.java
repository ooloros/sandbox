package ee.ooloros.uiback.basket;

import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.UUID;

@Repository
public class BasketRepository {

  Basket getBasket(String basketUuid) {
    return new Basket(basketUuid, Collections.singletonList(new BasketItem(UUID.randomUUID().toString())));
  }
}
