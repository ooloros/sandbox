package ee.ooloros.uiback.basket;

import ee.ooloros.uiback.SessionContext;
import org.springframework.stereotype.Service;

@Service
class BasketService {
  private final SessionContext sessionContext;
  private final BasketRepository basketRepository;

  BasketService(SessionContext sessionContext, BasketRepository basketRepository) {
    this.sessionContext = sessionContext;
    this.basketRepository = basketRepository;
  }

  Basket getBasket() {
    return basketRepository.getBasket(sessionContext.getOrCreateBasketUuid());
  }
}
