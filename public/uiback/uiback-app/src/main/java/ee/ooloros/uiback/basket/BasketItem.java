package ee.ooloros.uiback.basket;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BasketItem {
  private final String uuid;

  BasketItem(@JsonProperty("uuid") String uuid) {
    this.uuid = uuid;
  }

  public String getUuid() {
    return uuid;
  }
}
