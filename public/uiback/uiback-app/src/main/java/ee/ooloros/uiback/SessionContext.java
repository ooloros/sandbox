package ee.ooloros.uiback;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static org.springframework.web.context.WebApplicationContext.SCOPE_SESSION;

@Component
@Scope(value = SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionContext {
  private String basketUuid;

  public String getOrCreateBasketUuid() {
    if (basketUuid == null) {
      basketUuid = UUID.randomUUID().toString();
    }
    return basketUuid;
  }
}
