package ee.ooloros.uiback.basket;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/basket")
public class BasketController {
  private final BasketService basketService;

  BasketController(BasketService basketService) {
    this.basketService = basketService;
  }

  @GetMapping
  public @ResponseBody Basket getBasket() {
    return basketService.getBasket();
  }
}
