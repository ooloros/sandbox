package ee.ooloros.uiback.basket;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Basket {
  private final String uuid;
  private final List<BasketItem> basketItems;

  public Basket(@JsonProperty("uuid") String uuid,
                @JsonProperty("basketItems") List<BasketItem> basketItems) {
    this.uuid = uuid;
    this.basketItems = basketItems;
  }

  public String getUuid() {
    return uuid;
  }

  public List<BasketItem> getBasketItems() {
    return basketItems;
  }
}
