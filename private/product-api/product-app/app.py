#!/usr/bin/env python3
from flask import Flask
from flask import Response
from random import randint
from datetime import datetime, timedelta
from json import dumps, loads
from flask_pymongo import PyMongo
import uuid
DEFAULT_TO_JSON_SERIALISER=lambda o: getattr(o, '__dict__', str(o))
MEDIA_TYPE_PRODUCT_JSON = "application/vnd.ee.ooloros.product+json"
app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://mongo:mongo@database:27017/products?authSource=admin"
mongo = PyMongo(app)


class Product:
    def __init__(self, href:str, id: str, name: str, amount: int, amountUnit: str):
        self.href = href
        self.id = id
        self.name = name
        self.amount = amount
        self.amountUnit = amountUnit


def other():
    query_filter = {"id": "123"}
    resp = mongo.db.products.find(query_filter)
    return Response(dumps(resp), mimetype=MEDIA_TYPE_PRODUCT_JSON)


@app.route(rule='/products/<product_id>', methods=['GET'])
def get_product(product_id: str):
    query_filter = {
        "id": product_id
    }
    resp = mongo.db.products.find_one_or_404(query_filter)
    return Response(dumps(resp, default=DEFAULT_TO_JSON_SERIALISER), mimetype=MEDIA_TYPE_PRODUCT_JSON)


@app.route(rule='/products', methods=['POST'])
def create_product():
    random_uuid = str(uuid.uuid4())
    cart = Product(href="/products/" + random_uuid,
                   id=random_uuid,
                   name="COFFEE",
                   amount=1000,
                   amountUnit="g"
                )
    entry = loads(dumps(cart, default=DEFAULT_TO_JSON_SERIALISER))
    if randint(0, 1) == 1:
        entry = {
            'bla': 'bla-value',
            **entry
        }
    mongo.db.products.insert(entry)
    return Response(dumps(entry, default=DEFAULT_TO_JSON_SERIALISER), mimetype=MEDIA_TYPE_PRODUCT_JSON)


if __name__ == '__main__':
    print('App running')
    app.run(host='0.0.0.0')
