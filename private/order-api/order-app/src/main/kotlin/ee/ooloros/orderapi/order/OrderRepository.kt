package ee.ooloros.orderapi.order

import org.springframework.stereotype.Repository
import java.util.*

@Repository
class OrderRepository {
  fun getOrder(id: UUID): Order {
    return Order(
        id = id,
        customerId = 2L
    )
  }
}
