package ee.ooloros.orderapi.report

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.OffsetDateTime.now
import java.time.temporal.ChronoUnit

@RestController
@RequestMapping("/reports")
class ReportController {
    @GetMapping
    fun getReport(): ReportDTO {
        val start = now().truncatedTo(ChronoUnit.DAYS)
        return ReportDTO(reportRows = emptyList(), startDate = start, endDate = now())
    }
}