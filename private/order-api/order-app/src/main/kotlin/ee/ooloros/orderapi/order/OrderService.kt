package ee.ooloros.orderapi.order

import org.springframework.stereotype.Service
import java.util.*

@Service
class OrderService(
    private val orderRepository: OrderRepository
) {
  fun getOrder(id: UUID): Order {
    return orderRepository.getOrder(id)
  }
}
