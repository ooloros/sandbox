package ee.ooloros.orderapi.order

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/orders")
class OrderController(
    private val orderService: OrderService
) {
  @GetMapping("/{id}")
  fun getOrder(@PathVariable("id") id: UUID): OrderResource {
    val order = orderService.getOrder(id)
    return OrderResource(order = order)
  }
}