package ee.ooloros.orderapi.order

import com.fasterxml.jackson.annotation.JsonUnwrapped

data class OrderResource(
    @JsonUnwrapped val order: Order
)
