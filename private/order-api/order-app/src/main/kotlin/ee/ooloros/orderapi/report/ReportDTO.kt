package ee.ooloros.orderapi.report

import ee.ooloros.orderapi.common.Money
import java.time.OffsetDateTime

data class ReportDTO(
    val reportRows: List<ReportRowDto>,
    val startDate: OffsetDateTime,
    val endDate: OffsetDateTime,
) {
    companion object {
        val MIME_TYPE = "application/vnd.ee.ooloros.report+json;version=1"
    }
}

data class ReportRowDto(
    val orderId: Long,
    val customerName: String,
    val totalAmount: Money
)