package ee.ooloros.orderapi.order

import java.time.LocalDateTime
import java.util.*

data class Order(
    val id: UUID,
    val customerId: Long?,
    val createdAt: LocalDateTime = LocalDateTime.now()
)
