package ee.ooloros.orderapi.common

import java.math.BigDecimal

/**
 * NOT A GOOD IDEA!
 * you'd think that Money of all things could be common, but it is actually not
 * * this would translate to an api-common level class, after order-report split,
 *   but some other api's might not care about money - they don't need it
 *
 * * to be 100% honest, then it is not a 100% common thing between order-api and report-api either
 *   * in reporting-api we sometimes use kEUR or mEUR "currency" - this is specific to accounting
 *     and (after some multiplication) interchangeable with EUR
 *
 *   * in order-api EUR is the accepted currency and payments are always handled in EUR (never kEUR)
 *
 * these are minor differences, but point to a slightly different understanding of "commonly understandable" concepts
 * therefore it is better to handle Money differently on each API, but use the same domain-term Money
 * moreover this separate handling translates to loosely coupled API's
 */
data class Money(val amount: BigDecimal, val currency: String = "EUR") {
    companion object {
        val ZERO = Money(amount = BigDecimal.ZERO, currency = "EUR")
    }
}