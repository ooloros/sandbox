package ee.ooloros.orderapi.config

import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.utility.DockerImageName

class KContainer(imageName: DockerImageName): GenericContainer<KContainer>(imageName)

class KPostgresContainer(imageName: DockerImageName): PostgreSQLContainer<KPostgresContainer>(imageName)
