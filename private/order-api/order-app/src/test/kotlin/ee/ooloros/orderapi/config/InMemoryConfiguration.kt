package ee.ooloros.orderapi.config

import ee.ooloros.orderapi.common.Money
import org.springframework.beans.factory.DisposableBean
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import org.springframework.test.context.support.TestPropertySourceUtils
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.utility.DockerImageName
import java.math.BigDecimal

@Configuration
@Profile("in-memory")
class InMemoryConfiguration {

    @Bean
    fun mediumCoffeePrice(): Money {
        return Money(amount = BigDecimal.ONE, currency = "EUR")
    }

    @Bean
    fun mediumBurgerPrice(): Money {
        return Money(amount = BigDecimal.valueOf(5), currency = "EUR")
    }

    @Bean
    fun largeCoffeePrice(): Money {
        return Money(amount = BigDecimal.valueOf(2), currency = "EUR")
    }

    @Component
    @Profile("in-memory")
    @Lazy(false)
    class PostgresContainerHolder : ApplicationContextAware, DisposableBean{
        companion object {
            var applicationContext: ConfigurableApplicationContext? = null
            @Container
            @JvmStatic
            private val db = KPostgresContainer(DockerImageName.parse("postgres:11.5-alpine"))
                .withUsername("orderapi")
                .withDatabaseName("orderapi")
                .withPassword("orderapi_pw")
        }

        override fun setApplicationContext(context: ApplicationContext) {
            db.start()
            applicationContext = context as ConfigurableApplicationContext
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                applicationContext!!,
                "spring.datasource.url=" + db.jdbcUrl,
                "spring.datasource.username=" + db.username,
                "spring.datasource.password=" + db.password
            )
        }

        override fun destroy() {
            applicationContext = null
        }
    }
}

