package ee.ooloros.orderapi.dsl.domain

data class OrderItem(val item: String, val size: String, val quantity: Long = 1)