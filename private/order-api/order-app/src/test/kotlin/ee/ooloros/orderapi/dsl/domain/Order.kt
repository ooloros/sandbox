package ee.ooloros.orderapi.dsl.domain

import ee.ooloros.orderapi.common.Money

data class Order(val items: List<OrderItem>, val money: Money)