package ee.ooloros.orderapi

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.testcontainers.junit.jupiter.Testcontainers

@Configuration
@ComponentScan
class TestConfiguration