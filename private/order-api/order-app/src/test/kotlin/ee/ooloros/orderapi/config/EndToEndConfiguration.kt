package ee.ooloros.orderapi.config

import ee.ooloros.orderapi.common.Money
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import java.math.BigDecimal

@Configuration
@Profile("e2e")
class EndToEndConfiguration {

    @Bean
    fun mediumCoffeePrice(): Money {
        return Money(amount = BigDecimal.ONE, currency = "EUR")
    }

    @Bean
    fun mediumBurgerPrice(): Money {
        return Money(amount = BigDecimal.valueOf(5), currency = "EUR")
    }

    @Bean
    fun largeCoffeePrice(): Money {
        return Money(amount = BigDecimal.valueOf(2), currency = "EUR")
    }

}