package ee.ooloros.orderapi

import ee.ooloros.orderapi.common.Money
import ee.ooloros.orderapi.dsl.Dsl
import ee.ooloros.orderapi.dsl.DslTest
import ee.ooloros.orderapi.dsl.domain.OrderItem
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import java.math.BigDecimal

@Tag("corner-case")
@DslTest
class AcceptanceCornerCaseTest : Dsl() {

    @Test
    fun orderBurger() {
        val orderId = this.order(
            item = OrderItem(item = "BURGER", size = "M"),
            money = mediumCoffeePrice
        )
        this.assertOrder(
            orderId = orderId,
            items = listOf(OrderItem(item = "BURGER", size = "M")),
            status = "FINISHED",
            moneyBack = Money.ZERO
        )
    }

    @Test
    fun orderCoffeeWithSurplusPayment() {
        val orderId = this.order(
            item = OrderItem(item = "COFFEE", size = "M"),
            money = Money(amount = mediumCoffeePrice.amount + BigDecimal.ONE)
        )
        this.assertOrder(
            orderId = orderId,
            items = listOf(OrderItem(item = "COFFEE", size = "M")),
            status = "FINISHED",
            moneyBack = Money(amount = BigDecimal.ONE)
        )
    }

    @Test
    @Tag("corner-case")
    fun orderMultipleCoffees() {
        val orderId = this.order(
            item = OrderItem(item = "COFFEE", size = "M", quantity = 2),
            money = Money(amount = mediumCoffeePrice.amount.multiply(BigDecimal.valueOf(2)))
        )
        this.assertOrder(
            orderId = orderId,
            items = listOf(OrderItem(item = "COFFEE", size = "M", quantity = 2)),
            status = "FINISHED",
            moneyBack = Money.ZERO
        )
    }

    @Test
    @Tag("corner-case")
    fun orderDifferentItems() {
        val orderId = this.order(
            items = listOf(
                OrderItem(item = "COFFEE", size = "M"),
                OrderItem(item = "COFFEE", size = "L"),
            ),
            money = Money(amount = mediumCoffeePrice.amount.multiply(BigDecimal.valueOf(2)))
        )
        this.assertOrder(
            orderId = orderId,
            items = listOf(OrderItem(item = "COFFEE", size = "M", quantity = 2)),
            status = "FINISHED",
            moneyBack = Money.ZERO
        )
    }

    @Test
    @Tag("corner-case")
    fun orderCoffeeWithoutPaying() {
        val orderId = this.order(
            item = OrderItem(item = "COFFEE", size = "M"),
            money = Money.ZERO
        )
        this.assertOrder(
            orderId = orderId,
            items = listOf(),
            status = "NOT_PROCESSED",
            errorCode = "NO_PAYMENT"
        )
    }

    @Test
    @Tag("corner-case")
    fun orderCoffeeWithoutIngredients() {
        val orderId = this.order(
            item = OrderItem(item = "COFFEE", size = "M"),
            money = mediumCoffeePrice
        )
        this.assertOrder(
            orderId = orderId,
            items = listOf(),
            status = "NOT_PROCESSED",
            errorCode = "NO_INGREDIENTS"
        )
    }
}