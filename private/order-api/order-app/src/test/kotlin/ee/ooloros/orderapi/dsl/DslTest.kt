package ee.ooloros.orderapi.dsl

import ee.ooloros.orderapi.TestConfiguration
import ee.ooloros.orderapi.config.InMemoryConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.testcontainers.junit.jupiter.Testcontainers

@Target(AnnotationTarget.CLASS)
@Testcontainers
@SpringBootTest
@ContextConfiguration(classes = [TestConfiguration::class])
annotation class DslTest