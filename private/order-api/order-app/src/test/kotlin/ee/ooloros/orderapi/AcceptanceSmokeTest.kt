package ee.ooloros.orderapi

import ee.ooloros.orderapi.common.Money
import ee.ooloros.orderapi.dsl.Dsl
import ee.ooloros.orderapi.dsl.DslTest
import ee.ooloros.orderapi.dsl.domain.OrderItem
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("smoke")
@DslTest
class AcceptanceSmokeTest : Dsl() {

    @Test
    fun orderCoffee() {
        val orderId = this.order(
            item = OrderItem(item = "COFFEE", size = "M"),
            money = mediumCoffeePrice
        )
        this.assertOrder(
            orderId = orderId,
            items = listOf(OrderItem(item = "COFFEE", size = "M")),
            status = "FINISHED",
            moneyBack = Money.ZERO
        )
    }

    @Test
    fun generateReport() {
        val orderId = this.order(
            item = OrderItem(item = "COFFEE", size = "M"),
            money = mediumCoffeePrice
        )
        this.assertReport(

        )
    }
}