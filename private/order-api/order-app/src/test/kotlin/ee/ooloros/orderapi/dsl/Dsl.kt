package ee.ooloros.orderapi.dsl

import ee.ooloros.orderapi.common.Money
import ee.ooloros.orderapi.dsl.domain.Order
import ee.ooloros.orderapi.dsl.domain.OrderItem
import org.junit.Before
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class Dsl {
    @Autowired(required = false)
    lateinit var mediumCoffeePrice: Money

    @Autowired(required = false)
    lateinit var mediumBurgerPrice: Money

    @Autowired(required = false)
    lateinit var largeCoffeePrice: Money


    @Before
    fun reset() {
        println("Resetting Order state")
    }

    fun order(items: List<OrderItem>, money: Money): Long {
        return 1L
    }

    fun order(item: OrderItem, money: Money): Long {
        return 1L
    }

    fun order(order: Order): Long {
        return 1L
    }

    fun assertOrder(orderId: Long, items: List<OrderItem>, status: String, errorCode: String) {

    }

    fun assertOrder(orderId: Long, items: List<OrderItem>, status: String, moneyBack: Money) {

    }

    fun assertReport() {

    }
}


