notes:
* Always use the same term throughout systems (DDD). 
  DTO vs Resource - they more-or-less mean the same thing. One has to go.
* [LIFT principle](http://bguiz.github.io/js-standards/angularjs/application-structure-lift-principle/)
  * Locate quickly
  * Identify at a glance
  * Flattest possible structure (when you get to 7+, consider )
  * Try to stay dry
* API-commons - only acceptable if developed as a library (commonly isn't...both developed and acceptable)
  * major-minor-patch versioning (refactor might mean a major-version change)
  * support multiple major versions (e.g. when upgrading a dependency library)
  * not a requirement for new API's (recommended)
  * prefer multiple small libraries to one large 120k git-repo that does everything "common"
* use jib for container images