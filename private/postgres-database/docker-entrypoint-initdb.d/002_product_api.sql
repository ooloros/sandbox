CREATE ROLE product_api LOGIN PASSWORD 'product_secret' VALID UNTIL 'infinity';
CREATE DATABASE product_api WITH ENCODING='UTF8' OWNER=product_api CONNECTION LIMIT=-1;
