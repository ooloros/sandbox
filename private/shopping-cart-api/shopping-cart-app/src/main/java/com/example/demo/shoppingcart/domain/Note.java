package com.example.demo.shoppingcart.domain;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class Note {
  private String id;
  private String author;
  private OffsetDateTime date;
  private String text;
}
