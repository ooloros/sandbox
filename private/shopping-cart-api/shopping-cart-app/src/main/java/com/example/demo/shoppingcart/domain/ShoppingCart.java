package com.example.demo.shoppingcart.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.OffsetDateTime;
import java.util.Set;

@Data
@Document("shoppingcarts")
public class ShoppingCart {
  @Id
  @JsonProperty("_id")
  private String mongoId;
  private String id;
  private String href;
  private OffsetDateTime validFor;
  private Set<CartItem> items;
}
