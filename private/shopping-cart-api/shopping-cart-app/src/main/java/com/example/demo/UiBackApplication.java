package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UiBackApplication {

  public static void main(String[] args) {
    SpringApplication.run(UiBackApplication.class, args);
  }

}
