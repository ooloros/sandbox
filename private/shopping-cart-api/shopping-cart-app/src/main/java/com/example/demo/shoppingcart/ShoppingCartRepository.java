package com.example.demo.shoppingcart;

import com.example.demo.shoppingcart.domain.ShoppingCart;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static java.util.Collections.emptySet;

@Repository
@Transactional
@RequiredArgsConstructor
public class ShoppingCartRepository {
  private final MongoTemplate mongoTemplate;

  public ShoppingCart getShoppingCart(String id) {
    Query query = new Query(Criteria.where("id").is(id));
    return mongoTemplate.findOne(query, ShoppingCart.class);
  }

  public ShoppingCart createShoppingCart() {
    ShoppingCart cart = new ShoppingCart();
    cart.setId(UUID.randomUUID().toString());
    cart.setHref("/shopping-cart/" + cart.getId());
    cart.setItems(emptySet());
    mongoTemplate.insert(cart);
    return cart;
  }
}
