package com.example.demo.shoppingcart;

import com.example.demo.shoppingcart.domain.ShoppingCart;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/shopping-cart")
@RequiredArgsConstructor
public class ShoppingCartController {
  private final ShoppingCartService shoppingCartService;

  @GetMapping("/{id}")
  public ShoppingCart getShoppingCart(@PathVariable("id") String id) {
    return shoppingCartService.getShoppingCart(id);
  }

  @PostMapping
  public ShoppingCart createShoppingCart() {
    return shoppingCartService.createShoppingCart();
  }
}
