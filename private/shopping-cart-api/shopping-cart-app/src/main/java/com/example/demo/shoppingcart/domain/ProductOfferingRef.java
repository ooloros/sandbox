package com.example.demo.shoppingcart.domain;

import lombok.Data;

@Data
public class ProductOfferingRef {
  private String id;
  private String href;
  private String name;
}
