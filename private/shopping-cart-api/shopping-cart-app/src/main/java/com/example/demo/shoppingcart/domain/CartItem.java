package com.example.demo.shoppingcart.domain;

import lombok.Data;

import java.util.Set;

@Data
public class CartItem {
  private String id;
  private String action;
  private Integer quantity;
  private String status;
  private ProductOfferingRef productOfferingRef;
  private Set<Note> notes;
}
