package com.example.demo.shoppingcart;

import com.example.demo.shoppingcart.domain.ShoppingCart;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class ShoppingCartService {
  private final ShoppingCartRepository shoppingCartRepository;

  public ShoppingCart getShoppingCart(String id) {
    return shoppingCartRepository.getShoppingCart(id);
  }

  public ShoppingCart createShoppingCart() {
    return shoppingCartRepository.createShoppingCart();
  }
}
